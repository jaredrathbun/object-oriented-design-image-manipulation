package imagemanipulation;

import BlurPackage.GaussianFilter;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/23/20
 *
 * This class holds the components for a GUI and also a private class that holds
 * the data for an Image.
 */
public class GUI extends JFrame
{
    private Image img, grayscaleImg, invertedImg, mirrorImg,
        blackAndWhiteImg, blurImg;
    private JButton loadButton, saveButton, quitButton;
    private JPanel actionPanel, infoPanel, defaultCard, grayscaleCard,
        invertedCard, mirrorCard, blackAndWhiteCard, blurCard, cards;
    private JComboBox operationsBox;
    private String cardTracker;
    private final String comboBoxItems[] = { "Default", "Grayscale", 
        "Inverted Colors", "Mirror", "Black + White", "Gaussian Blur" };

    /**
     * Default Constructor which sets the title for the class and calls methods
     * to set up GUI.
     */
    public GUI()
    {
        // Create the title for the frame.
        super("Project 2 - GUIs and Event Driven Programming");

        // Set the layout to be a BorderLayout.
        setLayout(new BorderLayout());
        
        // Initialize the components of the GUI.
        initComponents();

        // Adds action buttons to the frame.
        setupActionPanel();

        // Adds a panel that displays every available hotkey.
        setupHotkeysPanel();   
        
        // Request focus for the hotkeys to work.
        requestFocus();
    }

    /**
     * This method initializes the components of the GUI, mainly the buttons and
     * panels.
     */
    private void initComponents()
    {
        loadButton = new JButton("Load Image");
        saveButton = new JButton("Save Image");
        quitButton = new JButton("Quit");   
        actionPanel = new JPanel();
        infoPanel = new JPanel();
        defaultCard = new JPanel();
        grayscaleCard = new JPanel();
        invertedCard = new JPanel();
        mirrorCard = new JPanel();
        blackAndWhiteCard = new JPanel();
        blurCard = new JPanel();
        cards = new JPanel();
        cards.setLayout(new CardLayout());
        operationsBox = new JComboBox(comboBoxItems);
    }

    /**
     * This method sets up the cards to allow for switching from the JComboBox.
     */
    private void setupCards()
    {
        cards.setLayout(new CardLayout());

        JLabel defaultLabel = new JLabel();
        defaultLabel.setIcon(new ImageIcon(scaleImage(img)));
        defaultCard.add(defaultLabel);
        cards.add(defaultCard, "Default");

        JLabel grayscaleLabel = new JLabel();
        grayscaleLabel.setIcon(new ImageIcon(scaleImage(grayscaleImg)));
        grayscaleCard.add(grayscaleLabel);
        cards.add(grayscaleCard, "Grayscale");

        JLabel invertedLabel = new JLabel();
        invertedLabel.setIcon(new ImageIcon(scaleImage(invertedImg)));
        invertedCard.add(invertedLabel);
        cards.add(invertedCard, "Inverted Colors");

        JLabel mirrorLabel = new JLabel();
        mirrorLabel.setIcon(new ImageIcon(scaleImage(mirrorImg)));
        mirrorCard.add(mirrorLabel);
        cards.add(mirrorCard, "Mirror");

        JLabel blackAndWhiteLabel = new JLabel();
        blackAndWhiteLabel.setIcon(new ImageIcon(scaleImage(blackAndWhiteImg)));
        blackAndWhiteCard.add(blackAndWhiteLabel);
        cards.add(blackAndWhiteCard, "Black + White");

        JLabel cartoonifyLabel = new JLabel();
        blurCard.add(cartoonifyLabel);
        cartoonifyLabel.setIcon(new ImageIcon(scaleImage(blurImg)));
        blurCard.add(cartoonifyLabel);
        cards.add(blurCard, "Gaussian Blur");
        
        add(cards, BorderLayout.CENTER);
    }
    
    /**
     * This method is a helper method to setupCards(). It scales the 
     * image by half its size.
     * 
     * @param originalImage The image to resize.
     * @return A resized version of the image.
     */
    private java.awt.Image scaleImage(Image originalImage)
    {
        int width = originalImage.getImage().getWidth();
        int height = originalImage.getImage().getHeight();
        return originalImage.getImage().getScaledInstance(width / 2, 
                height / 2, java.awt.Image.SCALE_SMOOTH);       
    }

    /**
     * This method performs the operations on the copies of images created when
     * the image is loaded into the program.
     */
    private void doOperations()
    {
        grayscaleImg.toGrayScale(); // Grayscale.
        invertedImg.toInvertedColors(); // Inverted Colors.
        mirrorImg.mirrorImage(); // Mirror the Image.
        blackAndWhiteImg.toBlackAndWhite(); // Black and White.
        blurImg.blurImage(); // Blur Image.
    }

    /**
     * This method sets up the right panel of the GUI, which shows a list of all
     * the available hotkeys to use.
     */
    private void setupHotkeysPanel()
    {
        JPanel hotkeysPanel = new JPanel();
        hotkeysPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        JLabel header = new JLabel("Hotkeys");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        hotkeysPanel.add(header, gbc);

        JTextArea hotkeys = new JTextArea("Load Image: CTRL + L"
                + "\nSave Image: CTRL + S"
                + "\nQuit Program: CRTL + Q"
                + "\nDefault: CTRL + 1"
                + "\nGrayscale: CTRL + 2"
                + "\nInverted: CTRL + 3"
                + "\nMirror: CTRL + 4"
                + "\nBlack + White: CTRL + 5 "
                + "\nGaussian Blur: CTRL + 6");
        hotkeys.setEditable(false);
        gbc.gridx = 0;
        gbc.gridy = 1;
        hotkeysPanel.add(hotkeys, gbc);

        add(hotkeysPanel, BorderLayout.EAST);
        
        // Add the keystroke to the frame for quitting the program.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Q, 
                KeyEvent.CTRL_DOWN_MASK), "quitAction");
        rootPane.getActionMap().put("quitAction", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        // Add a keystroke to the frame for saving the image.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 
                KeyEvent.CTRL_DOWN_MASK), "saveAction");
        rootPane.getActionMap().put("saveAction", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveImage();
            }
        });
        
        // Add a keystroke to the frame for load the image.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_L, 
                KeyEvent.CTRL_DOWN_MASK), "loadAction");
        rootPane.getActionMap().put("loadAction", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadImages();
            }
        });
        
        // Add a keystroke to the frame for switching to default.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_1, 
                KeyEvent.CTRL_DOWN_MASK), "default");
        rootPane.getActionMap().put("default", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) (cards.getLayout());
                cardLayout.show(cards, "Default");
                cardTracker = "Default";
                operationsBox.setSelectedIndex(0);
            }
        });
        
        // Add a keystroke to the frame for switching to grayscale.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_2, 
                KeyEvent.CTRL_DOWN_MASK), "grayscale");
        rootPane.getActionMap().put("grayscale", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) (cards.getLayout());
                cardLayout.show(cards, "Grayscale");
                cardTracker = "Grayscale";
                operationsBox.setSelectedIndex(1);
            }
        });
        
        // Add a keystroke to the frame for switching to inverted colors.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_3, 
                KeyEvent.CTRL_DOWN_MASK), "inverted");
        rootPane.getActionMap().put("inverted", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) (cards.getLayout());
                cardLayout.show(cards, "Inverted Colors");
                cardTracker = "Inverted Colors";
                operationsBox.setSelectedIndex(2);
            }
        });
        
        // Add a keystroke to the frame for switching to mirror.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_4, 
                KeyEvent.CTRL_DOWN_MASK), "mirror");
        rootPane.getActionMap().put("mirror", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) (cards.getLayout());
                cardLayout.show(cards, "Mirror");
                cardTracker = "Mirror";
                operationsBox.setSelectedIndex(3);
            }
        });
        
        // Add a keystroke to the frame for switching to Black and White.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_5, 
                KeyEvent.CTRL_DOWN_MASK), "blackandwhite");
        rootPane.getActionMap().put("blackandwhite", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) (cards.getLayout());
                cardLayout.show(cards, "Black + White");
                cardTracker = "Black + White";
                operationsBox.setSelectedIndex(4);
            }
        });
        
        // Add a keystroke to the frame for switching to Gaussian Blur.
        rootPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_6, 
                KeyEvent.CTRL_DOWN_MASK), "gaussianblur");
        rootPane.getActionMap().put("gaussianblur", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cardLayout = (CardLayout) (cards.getLayout());
                cardLayout.show(cards, "Gaussian Blur");
                cardTracker = "Gaussian Blur";
                operationsBox.setSelectedIndex(5);
            }
        });
    }

    /**
     * This method sets up the left panel of the GUI, which displays information
     * about the image as well as allows the user to select between the
     * different operations.
     */
    private void setupInfoAndOperationsPanel()
    {
        infoPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        int[] channelAverages = img.getRGBAverages();
        int[] imageSize = img.getResolution();

        JLabel header = new JLabel("Image Info");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        infoPanel.add(header, gbc);

        JTextPane infoArea = new JTextPane();
        infoArea.setEditable(false);
        infoArea.setText("Red Channel Average\n" + channelAverages[0]
                + "\nGreen Channel Average\n" + channelAverages[1]
                + "\nBlue Channel Average\n" + channelAverages[2]
                + "\nImage Width\n" + imageSize[0] + "px"
                + "\nImage Height\n" + imageSize[1] + "px"
                + "\nFilename\n" + img.getFilename());
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        infoPanel.add(infoArea, gbc);

        JLabel operationHeader = new JLabel("Operations");
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        infoPanel.add(operationHeader, gbc);

        operationsBox.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if (e.getStateChange() == ItemEvent.SELECTED)
                {
                    CardLayout c1 = (CardLayout) (cards.getLayout());
                    c1.show(cards, (String) e.getItem());
                    cardTracker = (String) e.getItem();
                }
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        infoPanel.add(operationsBox, gbc);

        add(infoPanel, BorderLayout.WEST);
    }

    /**
     * This method sets up the top panel of the GUI, which shows 3 buttons, one
     * for loading an image, one for saving the image, and the other for
     * quitting the program.
     */
    private void setupActionPanel()
    {
        actionPanel.setLayout(new FlowLayout());

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadImages();
            }

        });
        actionPanel.add(loadButton);
        
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveImage();
            }
        });
        saveButton.setEnabled(false);
        actionPanel.add(saveButton);

        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        actionPanel.add(quitButton);

        add(actionPanel, BorderLayout.NORTH);
    }

    /**
     * This method is a helper method the load button and the key listener 
     * for this class. It allows both the button and key listener to load 
     * the images.
     */
    private void loadImages()
    {
        // Prompt the user for the image.
        File file = promptForFile();

        // Create 6 copies of the image, for the different operations.
        img = new Image(file);
        grayscaleImg = new Image(file);
        invertedImg = new Image(file);
        mirrorImg = new Image(file);
        blackAndWhiteImg = new Image(file);
        blurImg = new Image(file);

        /* Setup the left panel, perform the operations on the image, 
        setup the cards, and then finally enable the save button.*/
        setupInfoAndOperationsPanel();
        doOperations();
        setupCards();
        saveButton.setEnabled(true);

        /* Switch the layout to the default layout. */
        CardLayout cl = (CardLayout) (cards.getLayout());
        cl.show(cards, "Default");

        cardTracker = "Default";

        validate();
    }
    /**
     * This method prompts the user for a image.
     *
     * @return The file the user chooses.
     */
    private File promptForFile()
    {
        // The accepted file types.
        String[] fileExtensions = { "jpg", "png" };

        // The object for the file chooser.
        JFileChooser display = new JFileChooser();

        // The file chooser title is set to msg.
        display.setDialogTitle("Please select an Image");

        // Set a file filter to open .jpg or .png images.
        display.setFileFilter(new FileNameExtensionFilter(
                "JPG/PNG Images (.jpg/.png)", fileExtensions));

        if (display.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
            return display.getSelectedFile();
        else
            return null;
    }

    /**
     * This method saves the image being currently viewed.
     */
    private void saveImage()
    {    
        // The object for the file chooser.
        JFileChooser fc = new JFileChooser();

        // Set the file chooser to only allow .png images.
        fc.setFileFilter(new FileNameExtensionFilter("PNG File (.png)", "png"));

        /* The user is prompted for a save location. If the file already exists
        they are prompted to make sure they want to overwrite. */
        if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            File selectedFile = fc.getSelectedFile();

            if (selectedFile.exists())
            {
                // Prompt the user for conformation to overwrite the file.
                int result = JOptionPane.showConfirmDialog(this,
                        "The file exists, overwrite?", "Existing file",
                        JOptionPane.YES_NO_CANCEL_OPTION);

                if (result == JOptionPane.YES_OPTION)
                    writeFile(selectedFile);
            } 
            else
                writeFile(selectedFile);
        }
    }

    /**
     * This method is a helper method to saveImage(). It finds the correct 
     * image and then saves it.
     *
     * @param img The Image to save.
     * @param location The location to save the Image to.
     */
    private void writeFile(File location)
    {
        BufferedImage imgToSave = null;
        
        // Find the correct image to save.
        switch (cardTracker)
        {
            case "Default":
                imgToSave = img.getImage();
                break;
            case "Grayscale":
                imgToSave = grayscaleImg.getImage();
                break;
            case "Inverted Colors":
                imgToSave = invertedImg.getImage();
                break;
            case "Mirror":
                imgToSave = mirrorImg.getImage();
                break;
            case "Black + White":
                imgToSave = blackAndWhiteImg.getImage();
                break;
            case "Gaussian Blur":
                imgToSave = blurImg.getImage();
                break;
        }
        
        /* Attempt to save the image and if successful, prompt the user 
        confirming it was created. */
        try
        {
            ImageIO.write(imgToSave, "png", location);
            JOptionPane.showMessageDialog(this, "Image successfully saved.");
        } catch (IOException ex)
        {
            System.err.println("Error saving image.");
        }
    }

    /**
     * This class holds the data for an Image object, which contains a
     * BufferedImage, the resolution of the image, the averages of each color
     * channel, and the filename.
     */
    private class Image
    {
        private BufferedImage img;
        private final int[] resolution;
        private final int[] RGB_AVERAGES;
        private final String FILE_NAME;

        /**
         * Constructor which takes a file as input and creates a new
         * BufferedImage from the file.
         *
         * @param image The file to create the BufferedImage from.
         */
        public Image(File image)
        {
            assert (image != null); // Precondition check.

            try
            {
                img = ImageIO.read(image);
            } catch (IOException | IllegalArgumentException e)
            {
                System.err.println("Unable to read image.");
            }

            // Get the height and width of the image.
            resolution = new int[2];
            resolution[0] = img.getWidth();
            resolution[1] = img.getHeight();

            // Calculate the averages of the r, g, and b values.
            RGB_AVERAGES = calculateChannelAverages();

            // Find the file name..
            FILE_NAME = image.getName();
        }

        /**
         * Getter for the image. (Useful for adding to labels).
         *
         * @return The BufferedImage created from the file.
         */
        public BufferedImage getImage()
        {
            return img;
        }

        /**
         * Getter for the resolution of the image.
         *
         * @return An array containing the resolution of the image.
         *
         */
        public int[] getResolution()
        {
            return resolution;
        }

        /**
         * Getter for the averages of each channel in the image (Red, Green,
         * Blue).
         *
         * @return An array containing three integers, one that represents each
         * channel's average.
         */
        public int[] getRGBAverages()
        {
            return RGB_AVERAGES;
        }

        /**
         * Getter for the file's name.
         *
         * @return The file's name.
         */
        public String getFilename()
        {
            return FILE_NAME;
        }

        /**
         * This method changes the color image to grayscale.
         */
        public void toGrayScale()
        {
            for (int i = 0; i < img.getWidth(); i++)
            {
                for (int j = 0; j < img.getHeight(); j++)
                {
                    // The image's pixel is set to the grayscale version of it.
                    img.setRGB(i, j, rgbToGray(img.getRGB(i, j)));
                }
            }
        }

        /**
         * This method inverts the image's colors by subtracting the value of
         * each pixel from 255.
         */
        public void toInvertedColors()
        {
            for (int x = 0; x < img.getWidth(); x++)
            {
                for (int y = 0; y < img.getHeight(); y++)
                {
                    img.setRGB(x, y, 255 - img.getRGB(x, y));
                }
            }
        }

        /**
         * This method converts the image to black and white only.
         * 
         * CODE REFERENCED FROM: 
         * https://memorynotfound.com/convert-image-black-white-java/
         */
        public void toBlackAndWhite()
        {
            // Create a copy of the image but only use 1-bit coloring.
            BufferedImage blackAndWhite = new BufferedImage(img.getWidth(),
                    img.getHeight(), BufferedImage.TYPE_BYTE_BINARY);

            // Set the background of the image to white if it is transparent.
            Graphics2D g = blackAndWhite.createGraphics();
            g.drawImage(img, 0, 0, Color.WHITE, null);
            g.dispose();

            // Copy the copy of the image onto the original image.
            for (int x = 0; x < img.getWidth(); x++)
            {
                for (int y = 0; y < img.getHeight(); y++)
                {
                    img.setRGB(x, y, blackAndWhite.getRGB(x, y));
                }
            }
        }

        /**
         * This method mirrors the image over the y-axis.
         * 
         * CODE REFERENCED FROM: 
         * https://stackoverflow.com/questions/9558981/flip-image-with-graphics2d
         */
        public void mirrorImage()
        {
            AffineTransform af = AffineTransform.getScaleInstance(-1, 1);
            AffineTransformOp atp = new AffineTransformOp(af,
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

            af.translate(-img.getWidth(), 0);

            atp = new AffineTransformOp(af,
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

            img = atp.filter(img, null);
        }

        /**
         * This method blurs the image using Median filtering.
         */
        public void blurImage()
        {
            // Create a copy of the image.
            BufferedImage copy = new BufferedImage(img.getWidth(),
                    img.getHeight(), img.getType());

            GaussianFilter gFilter = new GaussianFilter(5);
            copy = gFilter.filter(img, copy);
            
            // Copy the pixels of "copy" to "img".
            for (int x = 0; x < img.getWidth(); x++)
            {
                for (int y = 0; y < img.getHeight(); y++)
                {
                    img.setRGB(x, y, copy.getRGB(x, y));
                }
            }   
        }        

        /**
         * This method calculates the averages of each color channel in the
         * image (red, green, and blue).
         *
         * @return An array of length 3 with the first element being the
         * average of the red channel, second being the average of the green
         * channel, and last element being the blue channel's average.
         */
        private int[] calculateChannelAverages()
        {
            // An array to hold the averages of each channel.
            int[] averagesArray = new int[3];

            // A counter for each channel.
            int red = 0;
            int green = 0;
            int blue = 0;

            // A counter for the total number of pixels.
            int numOfPixels = 0;

            // Traverse the image and get the r, g, and b values of each pixel.
            for (int i = 0; i < img.getWidth(); i++)
            {
                numOfPixels++;

                for (int j = 0; j < img.getHeight(); j++)
                {
                    numOfPixels++;
                    red += ((img.getRGB(i, j) >> 16) & 0xFF);
                    green += ((img.getRGB(i, j) >> 8) & 0xFF);
                    blue += (img.getRGB(i, j) & 0xFF);
                }
            }

            // Calculate the averages of each color.
            averagesArray[0] = red / numOfPixels;
            averagesArray[1] = green / numOfPixels;
            averagesArray[2] = blue / numOfPixels;

            return averagesArray;
        }

        /**
         * This method returns the grayscale value of the entered RGB value.
         *
         * @param rgb An RGB value that is between 0 and 255.
         * @return The grayscale version of rgb.
         */
        private int rgbToGray(int rgb)
        {
            assert (rgb > 0) && (rgb < 256); // Precondition check.

            // The red channel.
            int redChannel = (int) Math.floor((getRedChannel(rgb)
                    + getGreenChannel(rgb)
                    + getBlueChannel(rgb)) / 3 + .5);

            // The green channel.
            int greenChannel = (int) Math.floor((getRedChannel(rgb)
                    + getGreenChannel(rgb)
                    + getBlueChannel(rgb)) / 3 + .5);

            // The blue channel.
            int blueChannel = (int) Math.floor((getRedChannel(rgb)
                    + getGreenChannel(rgb)
                    + getBlueChannel(rgb)) / 3 + .5);

            // The encoded grayscale value is returned.
            return encodeToRGB(redChannel, greenChannel, blueChannel);
        }

        /**
         * This method calculates the red value of an RGB value using bitwise
         * shifting.
         *
         * @param rgb The RGB value.
         *
         * @return The value of the red channel.
         */
        private int getRedChannel(int rgb)
        {
            assert (rgb > 0 && rgb < 256); // Precondition check.

            return ((rgb >> 16) & 0xFF);
        }

        /**
         * This method calculates the green value of an RGB value using bitwise
         * shifting.
         *
         * @param rgb The RGB value.
         *
         * @return The value of the green channel.
         */
        private int getGreenChannel(int rgb)
        {
            assert (rgb > 0 && rgb < 256); // Precondition check.

            return ((rgb >> 8) & 0xFF);
        }

        /**
         * This method calculates the blue value of an RGB value using bitwise
         * shifting.
         *
         * @param rgb The RGB value.
         *
         * @return The value of the blue channel.
         */
        private int getBlueChannel(int rgb)
        {
            assert (rgb > 0 && rgb < 256); // Precondition check.

            return (rgb & 0xFF);
        }

        /**
         * This method converts the three integers taken as input into an RGB
         * value using bitwise shifting.
         *
         * @param red The red value [0,255].
         * @param green The green value [0,255].
         * @param blue The blue value [0,255].
         *
         * @return the RGB value of the three channels.
         */
        private int encodeToRGB(int red, int green, int blue)
        {
            assert (red > 0 && red < 256) && (green > 0 && green < 256)
                    && (blue > 0 && blue < 256); // Precondition check.

            return ((red << 16) | (green << 8) | blue);
        }
    }
}