package imagemanipulation;

import javax.swing.JFrame;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/23/20
 *
 * This program demonstrates the use of GUI by displaying an image and allowing
 * a user to manipulate it.
 */

public class Driver
{
    public static void main(String[] args)
    {          
        // Create a new GUI object.
        GUI gui = new GUI();
        
        // Set the default operation when you click the 'X' to exit the program.
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set the size of the GUI.
        gui.setSize(1100, 900);
        
        // Set the visibility.
        gui.setVisible(true);

    }  
}


